<?php

use App\Models\Order;
use Slim\Http\Request;
use Slim\Http\Response;

/*
    * Root path API, this method is used for testing the api.
*/

$app->get(
    '/',
    function (Request $request, Response $response) {
        $response->getBody()->write("It works! Welcome to discount API");
        return $response;
    }
)->setName('root');

/*
    * This method calculates the discounts applied to a purchase order
    * Body payload example:
    * {
    *   "id": "3",
    *   "customer-id": "3",
    *   "items": [
    *     {
    *       "product-id": "A101",
    *       "quantity": "2",
    *       "unit-price": "9.75",
    *       "total": "19.50"
    *     }
    *   ],
    *   "total": "69.00"
    * }
*/

$app->post(
    '/discount/calculation',
    function (Request $request, Response $response) {
        $data        = $request->getParsedBody();
        $order       = $this->ordersManager->createOrder($data);
        $discounts   = $this->discountsManager->getDiscounts($order);
        $jsonContent = $this->serializer->serialize($discounts, 'json');

        $body = $response->getBody();
        $body->write($jsonContent);
        return $response;
    }
)->setName('giscount');
