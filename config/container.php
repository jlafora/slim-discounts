<?php

use App\Managers\CustomersManager;
use App\Managers\Discounts\DiscountsManager;
use App\Managers\Discounts\Rules\RuleTradeDiscount;
use App\Managers\Discounts\Rules\RuleGetOneFree;
use App\Managers\Discounts\Rules\RuleCheapestProductDiscount;
use App\Managers\OrdersManager;
use App\Managers\ProductsManager;
use App\Utils\DataLoader;
use App\Utils\ModelFactory;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

$container = $app->getContainer();

$container['serializer'] = function ($container) {
    $encoders    = [new JsonEncoder()];
    $normalizers = [
        new GetSetMethodNormalizer(),
        new ArrayDenormalizer(),
    ];

    return new Serializer($normalizers, $encoders);
};

$container['ModelFactory'] = function ($container) {
    return new ModelFactory();
};

$container['customersManager'] = function ($container) {
    $dataLoader = new DataLoader(__DIR__.'/../data/customers.json', 'App\Models\Customer[]', $container['serializer']);

    return new CustomersManager($dataLoader);
};

$container['productsManager'] = function ($container) {
    $dataLoader = new DataLoader(__DIR__.'/../data/products.json', 'App\Models\Product[]', $container['serializer']);

    return new ProductsManager($dataLoader);
};

$container['ordersManager'] = function ($container) {
    return new OrdersManager(
        $container['customersManager'],
        $container['productsManager'],
        $container['ModelFactory']
    );
};

$container['ruleTradeDiscount'] = function ($container) {
    $tradeDiscountConfig = ['minRevenue' => 1000, 'discountPercent' => 10];

    return new RuleTradeDiscount($tradeDiscountConfig, $container['ModelFactory']);
};

$container['ruleGetOneFree'] = function ($container) {
    $getOneFreeConfig = ['categoryId' => 2, 'numToGetOneFree' => 5];

    return new RuleGetOneFree($getOneFreeConfig, $container['ordersManager'], $container['ModelFactory']);
};

$container['ruleCheapestProductDiscount'] = function ($container) {
    $cheapestProductDiscountConfig = ['categoryId' => 1, 'numToGetDiscount' => 2, 'discountPercent' => 20];

    return new RuleCheapestProductDiscount(
        $cheapestProductDiscountConfig,
        $container['ordersManager'],
        $container['ModelFactory']
    );
};

$container['discountsManager'] = function ($container) {
    $discountsManager = new DiscountsManager();
    $discountsManager->addRule($container['ruleTradeDiscount']);
    $discountsManager->addRule($container['ruleGetOneFree']);
    $discountsManager->addRule($container['ruleCheapestProductDiscount']);

    return $discountsManager;
};
