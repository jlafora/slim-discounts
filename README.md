# Discounts
(MICRO)SERVICE that calculates discounts for orders.

URL: https://slim-discounts.aspgems.com

## Rules

There are three possible ways of getting a discount:

- A customer who has already bought for over € 1000, gets a discount of 10% on the whole order.
- For every product of category "Switches" (id 2), when you buy five, you get a sixth for free.
- If you buy two or more products of category "Tools" (id 1), you get a 20% discount on the cheapest product.

# Install and setup

## Installation with Docker

Previous steps: It's necessary to have installed [`docker-engine`](http://docs.docker.com/engine/installation/) and [`docker-compose`](https://docs.docker.com/compose/install/)

This API service is dockerized so, to have the service up and running in a local environment, it's as easy as executing these two commands:

* docker-compose build
* docker-compose up


## Installation without Docker

###Requeriments:
PHP 7.0 and up.

### Install and running
First, you need to install composer. See the [Download](https://getcomposer.org/download/) page for instructions.

Then, install php dependencies
```bash
composer install
```

and start your development server with
```bash
composer run start
```
## Composer scripts
|Script              |Description                            |
|--------------------|---------------------------------------|
|`composer start`    |It starts your dev server at 8080 port |
|`composer test`     |It runs your tests                     |
|`composer lint`     |It runs your linter                    |

## Using the API

### REST API endpoint list

|HTTP method  |URI path	                |Description                                            |
|-------------|-------------------------|-------------------------------------------------------|
|GET          | /                       |Root path API, this method is used for testing the api | 
|POST         | /discount/calculation   |Calculates the discounts applied to a purchase order   | 


### Example 1
```bash
curl -X POST \
  http://localhost:8080/discount/calculation \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
  "id": "2",
  "customer-id": "2",
  "items": [
    {
      "product-id": "B102",
      "quantity": "5",
      "unit-price": "4.99",
      "total": "24.95"
    }
  ],
  "total": "24.95"
}'
```

### Example 2
```bash
curl -X POST \
  http://localhost:8080/discount/calculation \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
  "id": "1",
  "customer-id": "1",
  "items": [
    {
      "product-id": "B102",
      "quantity": "10",
      "unit-price": "4.99",
      "total": "49.90"
    }
  ],
  "total": "49.90"
}'
```

### Example 3
```bash
curl -X POST \
  http://localhost:8080/discount/calculation \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
  "id": "3",
  "customer-id": "3",
  "items": [
    {
      "product-id": "A101",
      "quantity": "2",
      "unit-price": "9.75",
      "total": "19.50"
    },
    {
      "product-id": "A102",
      "quantity": "1",
      "unit-price": "49.50",
      "total": "49.50"
    }
  ],
  "total": "69.00"
}'
```

## Project Structure

### Folders:
* `config` - Configuration files.
* `data` - It contains data example files.
* `public` - Web server files (DocumentRoot). 
* `src` - PHP source code (The App namespace).
* `test` - It contains test files.
* `vendor` - Folder with all the library code. Reserved for composer.

### Files:
* `README.md`
* `bitbucket-pipelines.yml` - Configuration Bitcket pipeline.
* `composer.json` - Composer dependencies.
* `composer.lock` - Records the exact versions that are installed.
* `docker-compose.yml` - Defining and running of Docker containers.
* `Dockerfile` - Docker config file which is used to build a Docker image running this API.
* `phpcs.xml` - Configuration linter PHPCS.
* `phpunit.xml` - Configuration PHPUnit.