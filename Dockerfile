FROM composer:latest

ENV APP_HOME /app
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
ADD . /app
RUN composer install
CMD composer start