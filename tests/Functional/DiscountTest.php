<?php

namespace Tests\Functional;

class DiscountTest extends BaseTestCase
{

    /**
     * Test of RuleTradeDiscount
     *
     * @test
     */
    public function tradeDtoOk()
    {
        $dataJson = '
            {
              "id": "2",
              "customer-id": "2",
              "items": [
                {
                  "product-id": "B102",
                  "quantity": "5",
                  "unit-price": "4.99",
                  "total": "24.95"
                }
              ],
              "total": "24.95"
            }
        ';

        $response = $this->runApp('POST', '/discount/calculation', json_decode($dataJson, true));
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('TRADE_DTO', (string) $response->getBody());
    }

    /**
     * Test of RuleGetOneFree
     *
     * @test
     */
    public function ruleGetOneFree()
    {
        $dataJson = '
            {
              "id": "1",
              "customer-id": "1",
              "items": [
                {
                  "product-id": "B102",
                  "quantity": "10",
                  "unit-price": "4.99",
                  "total": "49.90"
                }
              ],
              "total": "49.90"
            }
        ';

        $response = $this->runApp('POST', '/discount/calculation', json_decode($dataJson, true));
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('GET_ONE_FREE', (string) $response->getBody());
    }

    /**
     * Test of RuleCheapestProductDiscount
     *
     * @test
     */
    public function ruleCheapestProductDiscount()
    {
        $dataJson = '
            {
              "id": "3",
              "customer-id": "3",
              "items": [
                {
                  "product-id": "A101",
                  "quantity": "2",
                  "unit-price": "9.75",
                  "total": "19.50"
                },
                {
                  "product-id": "A102",
                  "quantity": "1",
                  "unit-price": "49.50",
                  "total": "49.50"
                }
              ],
              "total": "69.00"
            }
        ';

        $response = $this->runApp('POST', '/discount/calculation', json_decode($dataJson, true));
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('CHEAPEST_PRODUCT_DTO', (string) $response->getBody());
    }
}
