<?php

namespace Tests\Unit\Managers\Discounts;

use App\Managers\ProductsManager;
use App\Utils\DataLoader;
use PHPUnit\Framework\TestCase;

class ProductsManagerTest extends TestCase
{
    /** @var DataLoader */
    private $dataLoader;

    public function setUp()
    {
        $this->dataLoader = $this->getMockBuilder(DataLoader::class)
            ->disableOriginalConstructor()
            ->setMethods(['getAll', 'getById'])
            ->getMock();
    }

    /**
     * @test
     */
    public function getProductsDataFromLoader()
    {
        $this->dataLoader->expects($this->once())
            ->method('getAll');

        $customersManager = new ProductsManager($this->dataLoader);
        $customersManager->getAll();
    }

    /**
     * @test
     */
    public function getOneCustomerDAtaFromLoader()
    {
        $productId = 2;
        $this->dataLoader->expects($this->once())
            ->method('getById')
            ->with($productId);

        $customersManager = new ProductsManager($this->dataLoader);
        $customersManager->getById($productId);
    }
}
