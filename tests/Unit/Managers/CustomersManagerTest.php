<?php

namespace Tests\Unit\Managers\Discounts;

use App\Managers\CustomersManager;
use App\Utils\DataLoader;
use PHPUnit\Framework\TestCase;

class CustomersManagerTest extends TestCase
{
    /** @var DataLoader */
    private $dataLoader;

    public function setUp()
    {
        $this->dataLoader = $this->getMockBuilder(DataLoader::class)
            ->disableOriginalConstructor()
            ->setMethods(['getAll', 'getById'])
            ->getMock();
    }

    /**
     * @test
     */
    public function getCustomersDataFromLoader()
    {
        $this->dataLoader->expects($this->once())
            ->method('getAll');

        $customersManager = new CustomersManager($this->dataLoader);
        $customersManager->getAll();
    }

    /**
     * @test
     */
    public function getOneCustomerDAtaFromLoader()
    {
        $customerId = 2;
        $this->dataLoader->expects($this->once())
            ->method('getById')
            ->with($customerId);

        $customersManager = new CustomersManager($this->dataLoader);
        $customersManager->getById($customerId);
    }
}
