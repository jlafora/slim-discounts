<?php

namespace Tests\Unit\Managers\Discounts;

use App\Managers\Discounts\DiscountsManager;
use App\Managers\Discounts\Rules\RuleInterface;
use App\Models\Order;
use PHPUnit\Framework\TestCase;

class DiscountsManagerTest extends TestCase
{
    /** @var Order */
    private $order;

    public function setUp()
    {
        $this->order = $this->createMock(Order::class);
    }

    /**
     * @test
     */
    public function getDiscountsUsingOneRule()
    {
        $rule = $this->getRuleMock();
        $rule->expects($this->once())
            ->method('checkDiscount')
            ->with($this->order);

        $discountsManager = new DiscountsManager();
        $discountsManager->addRule($rule);
        $discountsManager->getDiscounts($this->order);
    }

    /**
     * @test
     */
    public function getDiscountsUsingMoreThanOneRule()
    {
        $ruleOne = $this->getRuleMock();
        $ruleTwo = $this->getRuleMock();
        $ruleThree = $this->getRuleMock();

        $ruleOne->expects($this->once())
            ->method('checkDiscount')
            ->with($this->order);
        $ruleTwo->expects($this->once())
            ->method('checkDiscount')
            ->with($this->order);
        $ruleThree->expects($this->once())
            ->method('checkDiscount')
            ->with($this->order);
        $discountsManager = new DiscountsManager();
        $discountsManager->addRule($ruleOne);
        $discountsManager->addRule($ruleTwo);
        $discountsManager->addRule($ruleThree);
        $discountsManager->getDiscounts($this->order);
    }

    private function getRuleMock()
    {
        return $this->getMockBuilder(RuleInterface::class)
            ->setMethods(['checkDiscount'])
            ->getMock();
    }
}
