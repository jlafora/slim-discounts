<?php

namespace Tests\Unit\Managers\Discounts;

use App\Models\Discount;
use App\Models\Order;
use App\Models\OrderItem;
use App\Utils\ModelFactory;
use PHPUnit\Framework\TestCase;

class ModelFactoryTest extends TestCase
{
    /** @var ModelFactory */
    private $modelFactory;

    public function setUp()
    {
        $this->modelFactory = new ModelFactory();
    }

    /**
     * @test
     */
    public function canCreateOrderItem()
    {
        $orderItem = $this->modelFactory->createOrderItem();
        $this->assertInstanceOf(OrderItem::class, $orderItem);
    }

    /**
     * @test
     */
    public function canCreateOrder()
    {
        $order = $this->modelFactory->createOrder();
        $this->assertInstanceOf(Order::class, $order);
    }

    /**
     * @test
     */
    public function canCreateDiscount()
    {
        $discount = $this->modelFactory->createDiscount();
        $this->assertInstanceOf(Discount::class, $discount);
    }
}
