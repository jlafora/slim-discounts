<?php

namespace App\Managers;

use App\Models\Order;
use App\Utils\ModelFactory;

/**
 * Class OrdersManager
 *
 * @package App\Managers
 */
class OrdersManager
{
    private $customersManager;
    private $productsManager;
    private $modelFactory;

    /**
     * OrdersManager constructor.
     *
     * @param CustomersManager $customersManager
     * @param ProductsManager  $productsManager
     * @param ModelFactory     $modelFactory
     */
    public function __construct(
        CustomersManager $customersManager,
        ProductsManager $productsManager,
        ModelFactory $modelFactory
    ) {
        $this->customersManager = $customersManager;
        $this->productsManager  = $productsManager;
        $this->modelFactory     = $modelFactory;
    }

    /**
     * @param array $orderArray
     *
     * @return Order
     */
    public function createOrder(array $orderArray)
    {
        $order = $this->modelFactory->createOrder();
        $order->setId($orderArray['id']);
        $order->setCustomer($this->getCustomerBy($orderArray['customer-id']));
        $order->setTotal($orderArray['total']);
        $this->addOrderItems($order, $orderArray['items']);

        return $order;
    }

    /**
     * @param Order  $order
     * @param string $category
     *
     * @return array
     */
    public function getItemsByCategory(Order $order, string $category)
    {
        return array_filter(
            $order->getItems(),
            function ($element, $index) use ($category) {
                return ($element->getProduct()->getCategory() == $category);
            },
            ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * @param Order  $order
     * @param string $category
     *
     * @return array
     */
    public function getItemsByCategoryGroupProduct(Order $order, string $category)
    {
        $items = $this->getItemsByCategory($order, $category);
        $groupOrderItem = [];
        foreach ($items as $item) {
            $groupOrderItem[$item->getProduct()->getId()][] = $item;
        }

        return $groupOrderItem;
    }

    private function getCustomerBy($customerId)
    {
        return $this->customersManager->getById($customerId);
    }

    private function addOrderItems(Order &$order, $items)
    {
        if (!empty($items)) {
            foreach ($items as $item) {
                $orderItem = $this->createOrderItemFromItem($item);
                $order->addItem($orderItem);
            }
        }
    }

    private function createOrderItemFromItem($item)
    {
        $orderItem = $this->modelFactory->createOrderItem();
        $product   = $this->productsManager->getById($item['product-id']);
        $orderItem->setProduct($product);
        $orderItem->setQuantity($item['quantity']);
        $orderItem->setUnitPrice($item['unit-price']);
        $orderItem->setTotal($item['total']);

        return $orderItem;
    }
}
