<?php

namespace App\Managers;

use App\Utils\DataLoader;
use App\Utils\DataLoaderTrait;

/**
 * Class ProductsManager
 *
 * @package App\Managers
 */
class ProductsManager
{
    use DataLoaderTrait;

    /**
     * ProductsManager constructor.
     *
     * @param DataLoader $data
     */
    public function __construct(DataLoader $data)
    {
        $this->data = $data;
    }
}
