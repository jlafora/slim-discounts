<?php

namespace App\Managers;

use App\Utils\DataLoader;
use App\Utils\DataLoaderTrait;

/**
 * Class CustomersManager
 *
 * @package App\Managers
 */
class CustomersManager
{
    use DataLoaderTrait;

    /**
     * CustomersManager constructor.
     *
     * @param DataLoader $data
     */
    public function __construct(DataLoader $data)
    {
        $this->data = $data;
    }
}
