<?php

namespace App\Managers\Discounts;

use App\Managers\Discounts\Rules\RuleInterface;
use App\Models\Order;

/**
 * Class DiscountsManager
 *
 * @package App\Managers\Discounts
 */
class DiscountsManager
{
    private $rules;
    private $discounts;

    /**
     * DiscountsManager constructor.
     */
    public function __construct()
    {
        $this->rules = [];
        $this->discounts = [];
    }

    /**
     * @param RuleInterface $rule
     */
    public function addRule(RuleInterface $rule)
    {
        $this->rules[] = $rule;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    public function getDiscounts(Order $order)
    {
        foreach ($this->rules as $rule) {
            $ruleDiscount = $rule->checkDiscount($order);
            $this->addDiscount($ruleDiscount);
        }

        return $this->discounts;
    }

    private function addDiscount($ruleDiscount)
    {
        if (!empty($ruleDiscount)) {
            if (is_array($ruleDiscount)) {
                $this->discounts = array_merge($this->discounts, $ruleDiscount);
            } else {
                $this->discounts[] = $ruleDiscount;
            }
        }
    }
}
