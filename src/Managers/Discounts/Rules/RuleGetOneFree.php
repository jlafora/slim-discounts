<?php

namespace App\Managers\Discounts\Rules;

use App\Managers\OrdersManager;
use App\Models\Order;
use App\Utils\ArrayUtilsTrait;
use App\Utils\ModelFactory;

/**
 * For every product of category "Switches" (id 2), when you buy five, you get a sixth for free.
 *
 * Class RuleGetOneFree
 * @package App\Managers\Discounts\Rules
 */
class RuleGetOneFree implements RuleInterface
{
    use ArrayUtilsTrait;

    const TYPE = 'GET_ONE_FREE';

    private $categoryId;
    private $numToGetOneFree;
    private $orderManager;
    private $modelFactory;
    private $discounts;

    /**
     * RuleGetOneFree constructor.
     *
     * @param array         $config
     * @param OrdersManager $orderManager
     * @param ModelFactory  $modelFactory
     */
    public function __construct(array $config, OrdersManager $orderManager, ModelFactory $modelFactory)
    {
        $this->categoryId      = $config['categoryId'];
        $this->numToGetOneFree = $config['numToGetOneFree'];
        $this->orderManager    = $orderManager;
        $this->modelFactory    = $modelFactory;
        $this->discounts       = [];
    }

    /**
     * @param Order $order
     *
     * @return array|mixed
     */
    public function checkDiscount(Order $order)
    {
        $groupProducts  = $this->orderManager->getItemsByCategoryGroupProduct($order, $this->categoryId);

        foreach ($groupProducts as $group) {
            $numProducts = $this->sumPropObjects($group, 'getQuantity');
            $product = reset($group);
            $numToFree = intval($numProducts / ($this->numToGetOneFree + 1));
            if ($numToFree > 0) {
                $this->addDiscount($product, $numToFree);
            }
        }

        return $this->discounts;
    }

    private function addDiscount($product, $numToFree)
    {
        $discount = $this->modelFactory->createDiscount();
        $discount->setType(self::TYPE);
        $discount->setAmount($this->calculateAmount($product, $numToFree));
        $discount->setDescription($this->composeDescription($product, $numToFree));
        $discount->setExtra($this->extras($product, $numToFree));
        $this->discounts[] = $discount;
    }

    private function calculateAmount($product, $numToFree)
    {
        $amount = $product->getUnitPrice() * $numToFree;

        return round($amount, 2);
    }

    private function composeDescription($product, $numToFree)
    {
        return $numToFree." products free of category '".$product->getProduct()->getCategory()
        ."', when you buy ".($this->numToGetOneFree).", you get the next free.";
    }

    private function extras($product, $numToFree)
    {
        return [
            'productCategory' => $product->getProduct()->getCategory(),
            'product' => $product->getProduct()->getDescription(),
            'productQuantity' => $product->getQuantity(),
            'numToGetOneFree' => $this->numToGetOneFree,
            'numFree' => $numToFree
        ];
    }
}
