<?php

namespace App\Managers\Discounts\Rules;

use App\Models\Discount;
use App\Models\Order;
use App\Utils\ModelFactory;

/**
 * A customer who has already bought for over € 1000, gets a discount of 10% on the whole order.
 *
 * Class RuleTradeDiscount
 *
 * @package App\Managers\Discounts\Rules
 */
class RuleTradeDiscount implements RuleInterface
{
    const TYPE = 'TRADE_DTO';

    private $minRevenue;
    private $discountPercent;
    private $modelFactory;

    /**
     * RuleTradeDiscount constructor.
     *
     * @param array        $config
     * @param ModelFactory $modelFactory
     */
    public function __construct(array $config, ModelFactory $modelFactory)
    {
        $this->minRevenue      = $config['minRevenue'];
        $this->discountPercent = $config['discountPercent'];
        $this->modelFactory    = $modelFactory;
    }

    /**
     * @param Order $order
     *
     * @return Discount|null
     */
    public function checkDiscount(Order $order)
    {
        $discount = null;

        if ($order->getCustomer()->getRevenue() >= $this->minRevenue) {
            $discount = $this->modelFactory->createDiscount();
            $discount->setType(self::TYPE);
            $discount->setAmount($this->calculateAmount($order));
            $discount->setDescription($this->composeDescription());
            $discount->setExtra($this->extras($order));
        }

        return $discount;
    }

    private function calculateAmount(Order $order)
    {
        $amount = $order->getTotal() * ($this->discountPercent / 100);

        return round($amount, 2);
    }

    private function composeDescription()
    {
        return $this->discountPercent."% discount for having spent more than ".$this->minRevenue."$";
    }

    private function extras(Order $order)
    {
        return [
            'minRevenue' => $this->minRevenue,
            'customerRevenue' => $order->getCustomer()->getRevenue(),
            'discountPrecent' => $this->discountPercent
        ];
    }
}
