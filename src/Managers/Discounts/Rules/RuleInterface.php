<?php

namespace App\Managers\Discounts\Rules;

use App\Models\Order;

/**
 * Interface RuleInterface
 *
 * @package App\Managers\Discounts\Rules
 */
interface RuleInterface
{
    /**
     * @param Order $order
     *
     * @return mixed
     */
    public function checkDiscount(Order $order);
}
