<?php

namespace App\Managers\Discounts\Rules;

use App\Managers\OrdersManager;
use App\Models\Discount;
use App\Models\Order;
use App\Utils\ArrayUtilsTrait;
use App\Utils\ModelFactory;

/**
 * If you buy two or more products of category "Tools" (id 1), you get a 20% discount on the cheapest product.
 *
 * Class RuleCheapestProductDiscount
 *
 * @package App\Managers\Discounts\Rules
 */
class RuleCheapestProductDiscount implements RuleInterface
{
    use ArrayUtilsTrait;

    const TYPE = 'CHEAPEST_PRODUCT_DTO';

    private $categoryId;
    private $numToGetDiscount;
    private $discountPercent;
    private $orderManager;
    private $modelFactory;

    /**
     * RuleCheapestProductDiscount constructor.
     *
     * @param array         $config
     * @param OrdersManager $orderManager
     * @param ModelFactory  $modelFactory
     */
    public function __construct(array $config, OrdersManager $orderManager, ModelFactory $modelFactory)
    {
        $this->categoryId       = $config['categoryId'];
        $this->numToGetDiscount = $config['numToGetDiscount'];
        $this->discountPercent  = $config['discountPercent'];
        $this->orderManager     = $orderManager;
        $this->modelFactory     = $modelFactory;
    }

    /**
     * @param Order $order
     *
     * @return Discount|mixed|null
     */
    public function checkDiscount(Order $order)
    {
        $discount = null;
        $orderItems  = $this->orderManager->getItemsByCategory($order, $this->categoryId);
        $this->sortObjectsByProp($orderItems, 'getUnitPrice');
        $numProducts = $this->sumPropObjects($orderItems, 'getQuantity');

        if ($numProducts >= $this->numToGetDiscount) {
            $cheapestProduct = reset($orderItems);
            $discount = $this->modelFactory->createDiscount();
            $discount->setType(self::TYPE);
            $discount->setAmount($this->calculateAmount($cheapestProduct));
            $discount->setDescription($this->composeDescription($cheapestProduct));
            $discount->setExtra($this->extras($cheapestProduct, $numProducts));
        }

        return $discount;
    }

    private function calculateAmount($cheapestProduct)
    {
        $amount = $cheapestProduct->getUnitPrice() * ($this->discountPercent / 100);
        return round($amount, 2);
    }

    private function composeDescription($cheapestProduct)
    {
        return $this->discountPercent."% discount on the cheapest product "
        .$cheapestProduct->getProduct()->getDescription()
        .", category ".$cheapestProduct->getProduct()->getCategory();
    }

    private function extras($cheapestProduct, $numProducts)
    {
        return [
            'productCategory'  => $cheapestProduct->getProduct()->getCategory(),
            'cheapestProduct'  => $cheapestProduct->getProduct()->getDescription(),
            'productQuantity'  => $numProducts,
            'numToGetDiscount' => $this->numToGetDiscount
        ];
    }
}
