<?php

namespace App\Utils;

/**
 * Trait ArrayUtilsTrait
 *
 * @package App\Utils
 */
trait ArrayUtilsTrait
{
    /**
     * Sort a array of objects by atribute value
     *
     * @param   array  $items
     * @param   String $method The name of the parameter to sort by
     * @param   String $order The sort order
     *
     * @return  array
     */
    public function sortObjectsByProp($items, $method, $order = 'asc')
    {
        usort(
            $items,
            function ($a, $b) use ($method, $order) {
                $cmp = strcmp($a->$method(), $b->$method());
                return ($order === 'asc') ? $cmp : -$cmp;
            }
        );

        return $items;
    }

    /**
     * Sum atribute value a array of objects
     *
     * @param   array  $items
     * @param   String $method The name of the parameter to sum
     *
     * @return  float
     */
    public function sumPropObjects($items, $method)
    {
        return array_reduce(
            $items,
            function ($carry, $item) use ($method) {
                $carry += $item->$method();
                return $carry;
            },
            0
        );
    }
}
