<?php
namespace App\Utils;

use Symfony\Component\Serializer\Serializer;

/**
 * Class DataLoader
 *
 * @package App\Utils
 */
class DataLoader
{
    private $data;

    /**
     * DataLoader constructor.
     *
     * @param string     $file
     * @param string     $type
     * @param Serializer $serializer
     */
    public function __construct(string $file, string $type, Serializer $serializer)
    {
        $this->data = $serializer->deserialize(file_get_contents($file), $type, 'json');
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->data;
    }

    /**
     * @param mixed $id
     *
     * @return object
     */
    public function getById($id)
    {
        $result = null;

        foreach ($this->data as $object) {
            if ($id == $object->getid()) {
                $result = $object;
                break;
            }
        }

        return $result;
    }
}