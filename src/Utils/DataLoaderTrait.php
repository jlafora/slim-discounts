<?php

namespace App\Utils;

/**
 * Trait DataLoaderTrait
 *
 * @package App\Utils
 */
trait DataLoaderTrait
{
    /** @var DataLoader */
    private $data;

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->data->getAll();
    }

    /**
     * @param mixed $id
     *
     * @return mixed
     */
    public function getById($id)
    {
        return $this->data->getById($id);
    }
}
