<?php
namespace App\Utils;

use App\Models\Discount;
use App\Models\OrderItem;
use App\Models\Order;

/**
 * Class ModelFactory
 *
 * @package App\Utils
 */
class ModelFactory
{
    /**
     * @return OrderItem
     */
    public function createOrderItem()
    {
        return new OrderItem();
    }

    /**
     * @return Order
     */
    public function createOrder()
    {
        return new Order();
    }

    /**
     * @return Discount
     */
    public function createDiscount()
    {
        return new Discount();
    }
}
